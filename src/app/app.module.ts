import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {FileManagerModule} from 'ng6-file-man';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FileManagerModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
